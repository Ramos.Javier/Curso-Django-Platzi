"""Posts views."""

#Django
from django.contrib.auth.decorators import login_required
from django.shortcuts import render

#Utilities
from datetime import datetime

posts = [
    {
        'title' : 'Via Lactea',
        'user' : {
            'name' : 'Diego',
            'picture' : 'https://picsum.photos/60/60/?image=1'
        },
        'timestamp' : datetime.now().strftime('%b %dth, %Y - %H:%M hrs'),
        'photo' : 'https://picsum.photos/200/200/?image=1035',
    },
    {
        'title' : 'Frente de todos',
        'user' : {
            'name' : 'Mauricio',
            'picture' : 'https://picsum.photos/60/60/?image=2'
        },
        'timestamp' : datetime.now().strftime('%b %dth, %Y - %H:%M hrs'),
        'photo' : 'https://picsum.photos/200/200/?image=1036',
    },
    {
        'title' : 'Libertarios',
        'user' : {
            'name' : 'Pepe',
            'picture' : 'https://picsum.photos/60/60/?image=3'
        },
        'timestamp' : datetime.now().strftime('%b %dth, %Y - %H:%M hrs'),
        'photo' : 'https://picsum.photos/200/200/?image=1037',
    }
]

@login_required
def list_posts(request):
    """List existing posts-"""   
    return render(request, 'posts/feed.html', {'posts' : posts})