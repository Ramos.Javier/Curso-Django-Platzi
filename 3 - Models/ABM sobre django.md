# ABM sobre django

```py
from posts.models import User
# Para crear y prersistir en la BD
javi = User.objects.create( email='javier@gmail.com', password='12345', first_name='javier', last_name='ramos' )

# Para editarlo
javi.email='ramos.javier@gmail.com'
javi.save()

# Otra forma de crar o registrar un usuario en la BD
diego = User()
diego.email='diego@gmail.com'
diego.password='10'
diego.first_name='diego'
diego.last_name='maradona'
diego.is_admin=True
diego.save()

# Para eliminar un registro
javi.delete()

#para traer un usuario en particular
user = User.objects.get(email="diego@gmail.com")
print(user.last_name)

# Para filtrar los email que terminan en ''
usuarios_filtrados = User.objects.filter(email__endwith='@platzi.com')

# Para todos los usuarios
mis_usuarios = User.objects.all()

# Para actualizar todo un set de usuario, en este caso filtrados
User.objects.filter(email__endwith='@platzi.com').update(is_admin=True)


```


