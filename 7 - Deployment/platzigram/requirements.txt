asgiref==3.4.1
backports.zoneinfo==0.2.1
Django==4.0
django-mysql==4.3.0
Pillow==8.4.0
PyMySQL==1.0.2
sqlparse==0.4.2