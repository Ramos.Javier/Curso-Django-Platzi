"""Platzigram middlerware catalog."""

# Django
from django.shortcuts import redirect
from django.urls import reverse

# Models
from users.models import Profile

class ProfileCompletionMiddlerware:
    """Profile completion middlerware

    Ensure every user that is  interacting with the platform
    have their profile picture and biography.
    """

    def __init__(self, get_response):
        """Middlerware initialization."""
        self.get_response = get_response


    def __call__(self, request):
        """Code to be executed for each request before th view is called."""
        if not request.user.is_anonymous:
            profile = request.user.profile
            if not profile.picture or not profile.biography:
                if request.path not in [reverse('users:update'), reverse('users:logout')]:
                    return redirect('users:update')

        response = self.get_response(request)
        return response
